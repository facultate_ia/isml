# ISML
1. [Cursuri](./Cursuri/Cursuri.pdf) sunt toate cursurile concatenate
2. [Laboratoare](./Laboratoare/Laboratoare.pdf) sunt toate laboratoarele concatenate
3. [Note Vizualizare Varianta1](./Laboratoare/TemeVarianta1/NoteVizualizare.pdf) sunt notele la fiecare tema cu observatiile aferente
4. [Note Vizualizare Varianta2](./Laboratoare/TemeVarianta2/NoteISML.pdf) sunt notele la fiecare tema cu observatiile aferente
5. [ISML_answers_decrypt.ipynb](ISML_answers_decrypt.ipynb) ajuta la primele 4-5 teme, la exercitiile "grila".

Se copiaza stringul de la inceputul fisierului (exemplu statinf1.ipynb) ![cod_statinf1](images/cod_statinf1.png) si se pune in loc de stringul de la `answers`

![cod_answers](images/cod_answers.png) si se ruleaza notebook-ul pentru a afisa rezultatele.
